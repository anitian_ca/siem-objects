#!/usr/bin/env python3

import json,os,sys,requests,glob,argparse,re,getpass

def put_watch(watch_path):
    watch_exists = False
    watch_file = open(watch_path, "r")
    watch_json = re.sub(pattern = "\[CLIENT\]", repl = client, string = watch_file.read())
    watch_name = os.path.basename(watch_file.name)[:-5]
    URL = "https://%s:%s/_xpack/watcher/watch/%s" % (host, port, watch_name)
    watch_file.close()
    if debug >= 1:
        print("----------Operating on watch "+watch_name+"----------")
    if debug == 2:
        print(watch_json)
    try:
        request = requests.post(url = URL, data = watch_json, auth = requests.auth.HTTPBasicAuth(user, password), headers={'Content-Type': 'application/json'}, verify=False)
        if debug >= 1:
            print("\t"+json.dumps(request.json()))
    except requests.exceptions.ConnectionError:
        print("[ERROR]\tCould not connect to Elastic API endpoint "+URL)

def get_watches(base_path):
    for watch_path in os.listdir(base_path):
        if watch_path.endswith(".json"):
            path = "%s%s" % (base_path, watch_path)
            if debug > 1:
                print(path)
            put_watch(base_path+watch_path)

parser = argparse.ArgumentParser(description="Add Watch alerts to a SIEM")
parser.add_argument("--user", dest="user", help="User to connect to SIEM", default="elastic")
parser.add_argument("--passwd", dest="passwd", help="Password for defined user, NOT RECOMMENDED, please provide password at runtime if possible", default="DEFAULT")
parser.add_argument("--host", dest="host", help="Host to connect to SIEM", default="localhost")
parser.add_argument("--path", dest="path", help="Path to JSON Watch Alerts", default="./")
parser.add_argument("--port", dest="port", help="Port to use to connect to SIEM", default="9200")
parser.add_argument("--client", dest="client", help="Client to replace tags in the alert", default="anitian")
parser.add_argument("--debug", dest="debug", help="Debug level to report to stdout, possible values 0 (no output), 1 (standard output), 2 (verbose output)", default=1, type=int, choices=[0,1,2])
args = parser.parse_args()
user = args.user
host = args.host
path = args.path
port = args.port
passwd = args.passwd
debug = args.debug
client = args.client
if passwd == "DEFAULT":
    password = getpass.getpass("Password for "+user+": ")
get_watches(path)
